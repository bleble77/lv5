﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad1_2
{
    class ShippingService
    {
        private double PricePerKg;
        
        public ShippingService(double PricePerKg)
        {
            this.PricePerKg = PricePerKg;
        }

         public double ShippingPrice(Box package){     
             double PackagePrice = PricePerKg * package.Weight; 
             return PackagePrice;
        }
        //u metodi ShippingPrice dvoumio sam se izmedju koristenja IShipable ili Box
        //na kraju sam se odlucio za box jer mi je to logicnije da se sve salje u kutijama

    }
}
