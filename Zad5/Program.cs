﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad5
{
    class Program
    {
        static void Main(string[] args)
        {
            ITheme blackAndYellow = new WizKhalifaTheme();
            ReminderNote reminder = new ReminderNote("You know what it is",blackAndYellow);
            reminder.Show();
        }
    }
}
